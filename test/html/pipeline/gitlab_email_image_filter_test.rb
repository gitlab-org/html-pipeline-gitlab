require 'test_helper'
require 'html/pipeline/gitlab'
require 'uri'

# Test for the GitlabEmailImageFilter class
class HTML::Pipeline::GitlabEmailImageFilterTest < Minitest::Test
  GitlabEmailImageFilter = HTML::Pipeline::Gitlab::GitlabEmailImageFilter

  def test_prevent_path_traversal
    context = {
      base_url: 'https://foo.com/namespace/project/uploads',
      upload_path: '/opt/gitlab/public/uploads/namespace/project'
    }

    filter =
      GitlabEmailImageFilter.new(
        '<img src="https://foo.com/namespace/project/uploads/1234/test.jpg" />',
        context)

    {
      '/..' => '/', '/a/../b' => '/a/b', '/a/../b/' => '/a/b/',
      '/%2e.' => '/', '/a/%2E%2e/b' => '/a/b', '/a%2f%2E%2e%2Fb/' => '/a/b/',
      '//' => '/', '/%2fetc%2Fpasswd' => '/etc/passwd'
    }.each do |a, b|
      filtered_a = filter.prevent_path_traversal(a)
      assert_match filtered_a, b
    end
  end

  def test_file_path
    context = {
      base_url: 'https://foo.com/namespace/project/uploads',
      upload_path: '/opt/gitlab/public/uploads/namespace/project'
    }

    img_src =
      'https://foo.com/namespace/project/uploads/1234/test.jpg'

    filter =
      GitlabEmailImageFilter.new(
        '<img src="https://foo.com/namespace/project/uploads/1234/test.jpg" />',
        context)

    file_path =
      filter.get_file_path(img_src,
                           context[:upload_path],
                           context[:base_url])
    expected_file_path =
      '/opt/gitlab/public/uploads/namespace/project/1234/test.jpg'

    assert_match file_path, expected_file_path
  end
end
